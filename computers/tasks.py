from celery.decorators import task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@task(bind=True, name="process_log_file")
def process_log_file(self, lpk):
    """Processes Log File"""
    from .models import Log

    logger.info("Sent application receipt email.")
    try:
        log = Log.objects.get(pk=lpk)
    except Log.DoesNotExist as ex:
        logger.warning(str(ex))
        self.retry(countdown=10)
    else:
        log.process()
        return "Processed"

