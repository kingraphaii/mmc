
class BarChart(object):
    title = ""
    labels = []
    datasets = []

    def __init__(self, title=""):
        self.title = title
        self.labels = []
        self.datasets = []
