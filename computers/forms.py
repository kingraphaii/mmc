from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordChangeForm
from django.conf import settings

class AuthForm(AuthenticationForm):
    def confirm_login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                "This account is inactive.",
                code='inactive',
            )
        if not user.is_staff:
            raise forms.ValidationError(
                "Access denied.", code="not_allowed"
            )

class CustomPasswordChangeForm(PasswordChangeForm):
    pass
