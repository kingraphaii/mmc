from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.contrib.auth import views as auth_views, logout
from django.urls import reverse_lazy
from django import http

from .models import Computer, Log, Branch, Event, LogEntry
from .forms import AuthForm, CustomPasswordChangeForm
from .mixins import LoginRequiredMixin


class Base(LoginRequiredMixin):
    pass


class Login(auth_views.LoginView):
    template_name = "login.html"
    authentication_form = AuthForm

    def get_success_url(self):
        return reverse_lazy("com:index")


class Logout(LoginRequiredMixin, generic.View):
    def get(self, request):
        logout(request)
        return http.HttpResponseRedirect(redirect_to=self.get_login_url())


class Index(Base, generic.TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        ctx  = super().get_context_data(**kwargs)
        asset_count = Computer.objects.count()
        branch_count = Branch.objects.count()
        log_count = Log.objects.count()
        ctx.update({
            "asset_count": asset_count,
            "branch_count": branch_count,
            "log_count": log_count,
            "recent_logs": Log.objects.order_by("-created")[:10],
        })
        return ctx


class LogDetail(Base, generic.DetailView):
    template_name = "log.html"
    model = Log
    context_object_name = "log"


class Logs(Base, generic.ListView):
    template_name = "logs.html"
    model = Log
    context_object_name = "logs"


class Branches(Base, generic.ListView):
    model = Branch
    context_object_name = "brs"
    template_name = "branches.html"


class BranchDetail(Base, generic.DetailView):
    template_name = "branch.html"
    model = Branch
    context_object_name = "br"


class ChangePwd(Base, auth_views.PasswordChangeView):
    template_name = "cp.html"
    form_class = CustomPasswordChangeForm

    def get_success_url(self):
        return reverse_lazy("com:login")


class Computers(Base, generic.ListView):
    template_name = "comps.html"
    model = Computer
    context_object_name = "comps"


class ComputerDetail(Base, generic.DetailView):
    template_name = "comp.html"
    model = Computer
    context_object_name = "comp"

    
    def get_context_data(self, **kwargs):
        from . import reports
        ctx = super().get_context_data(**kwargs)

        # metrics by branch
        mbb = reports.BarChart(title="Metrics by Branch")
        mbb.labels = [b.name for b in Branch.objects.all()]
        for e in Event.objects.all():
            ds = {
                "label": '%s' % e.title,
                "backgroundColor": '%s' % e.color,
                "borderColor": '%s'% e.color,
                "pointColor": '#3b8bba',
                "pointStrokeColor": 'rgba(60,141,188,1)',
                "pointHighlightFill": '#fff',
                "pointHighlightStroke": 'rgba(60,141,188,1)',
                "data" : [LogEntry.objects.filter(event=e, log__computer__branch=b).count() for b in Branch.objects.all()], 
            }
            mbb.datasets.append(ds)

        ctx.update({
            "events": Event.objects.all(),
            "mbb": mbb,
        })
        return ctx


class Events(Base, generic.ListView):
    template_name = "events.html"
    context_object_name = "events"
    model = Event

class EventDetail(Base, generic.DetailView):
    template_name = "event.html"
    context_object_name = "ev"
    model = Event



class Reports(Base, generic.TemplateView):
    template_name = "reports.html"

    def get_context_data(self, **kwargs):
        from . import reports
        ctx = super().get_context_data(**kwargs)

        # metrics by branch
        mbb = reports.BarChart(title="Metrics by Branch")
        mbb.labels = [b.name for b in Branch.objects.all()]
        for e in Event.objects.all():
            ds = {
                "label": '%s' % e.title,
                "backgroundColor": '%s' % e.color,
                "borderColor": '%s'% e.color,
                "pointColor": '#3b8bba',
                "pointStrokeColor": 'rgba(60,141,188,1)',
                "pointHighlightFill": '#fff',
                "pointHighlightStroke": 'rgba(60,141,188,1)',
                "data" : [LogEntry.objects.filter(event=e, log__computer__branch=b).count() for b in Branch.objects.all()], 
            }
            mbb.datasets.append(ds)

        ctx.update({
            "events": Event.objects.all(),
            "mbb": mbb,
        })
        return ctx

