# Generated by Django 2.2.7 on 2020-09-25 23:27

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('computers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('event_id', models.CharField(max_length=10, unique=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'events',
                'verbose_name_plural': 'events',
            },
        ),
        migrations.AlterModelOptions(
            name='branch',
            options={'verbose_name': 'branch', 'verbose_name_plural': 'branches'},
        ),
        migrations.RemoveField(
            model_name='log',
            name='log_type',
        ),
        migrations.RemoveField(
            model_name='log',
            name='name',
        ),
        migrations.AddField(
            model_name='log',
            name='event',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='logs', to='computers.Event'),
        ),
    ]
