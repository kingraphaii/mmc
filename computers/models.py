import csv
from io import StringIO
import datetime

from django.core.validators import ValidationError
from django.db import models
from django.utils.timezone import make_aware

from model_utils.models import TimeStampedModel


class Branch(TimeStampedModel):
    name = models.CharField(max_length=255, help_text="e.g Teechez Gweru Main")
    
    class Meta:
        verbose_name = "branch"
        verbose_name_plural = "branches"
    
    def __str__(self):
        return self.name


class Computer(TimeStampedModel):
    TYPE_SERVER = "SERVER"
    TYPE_WORKSTATION = "WORKSTATION"
    TYPE_LAPTOP = "LAPTOP"
    COMPUTER_TYPE_CHOICES = (
        (TYPE_SERVER, "Server"),
        (TYPE_WORKSTATION, "Workstation"),
        (TYPE_LAPTOP, "Laptop"),
    )

    branch = models.ForeignKey(to="Branch", on_delete=models.CASCADE, related_name="computers")
    computer_type = models.CharField(
        max_length=255, choices=COMPUTER_TYPE_CHOICES,
    )
    serial = models.CharField(
        max_length=255, unique=True
    )
    nickname = models.CharField(
        max_length=255, help_text="e.g Blade Mail Servier/Reception Workstation"
    )

    class Meta:
        verbose_name = "computer"
        verbose_name_plural = "computers"
    
    def __str__(self):
        return '%s > %s' % (self.serial, self.branch) 
    
    @property
    def type(self):
        return self.computer_type
    
    @type.setter
    def type(self, v):
        self.computer_type = v


class Event(TimeStampedModel):
    event_id = models.CharField(max_length=10, unique=True)
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255,)
    color = models.CharField(null=True, blank=False, max_length=255)

    class Meta:
        verbose_name = "events"
        verbose_name_plural = "events"
    
    def __str__(self):
        return "%s - %s" % (self.event_id, self.title)


class Log(TimeStampedModel):
    computer = models.ForeignKey(
        to="Computer",
        on_delete=models.CASCADE,
        related_name="logs",
    )

    file = models.FileField()
    start = models.DateTimeField(null=True, blank=False)
    end = models.DateTimeField(null=True, blank=False)

    processed = models.BooleanField(default=False, editable=False)

    class Meta:
        verbose_name = "log"
        verbose_name_plural = "logs"
    
    def __str__(self):
        return "%s - %s" % (self.computer.serial, self.computer.nickname)

    def clean(self):
        super().clean()
        if all([self.start, self.end]):
            if self.start > self.end:
                raise ValidationError("Start should be before end!")

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    def process(self):
        [_.delete() for _ in self.entries.all()]
        file = self.file.read().decode('utf-8')
        csv_data = csv.reader(StringIO(file), delimiter=',')
        line_count = 0
        for row in csv_data:
            if line_count == 0:
                pass
            else:
                event_id = row[3]
                event_obj, _ = Event.objects.get_or_create(
                    event_id=event_id,
                    defaults={
                        "title": row[4],
                        "description": "-",
                    }
                )

                details = row[5]

                timestamp_raw = row[1]
                timestamp = make_aware(datetime.datetime.strptime(timestamp_raw, "%d/%m/%Y %I:%M:%S %p"))
                
                self.entries.create(
                    event=event_obj,
                    details=details,
                    timestamp=timestamp
                )
                print("hhh", event_obj)
            line_count += 1
        self.mark_processed()

    def mark_processed(self):
        self.processed = True
        self.save()


class LogEntry(TimeStampedModel):
    log = models.ForeignKey(
        to="Log",
        on_delete=models.CASCADE,
        related_name="entries",
        null=True, blank=False,
    )
    event = models.ForeignKey(
        to="Event",
        related_name="logs",
        on_delete=models.CASCADE,
        null=True, blank=False,
    )
    details = models.TextField(null=True, blank=True,)
    timestamp = models.DateTimeField()

    class Meta:
        verbose_name = "log entry"
        verbose_name_plural = "logs entries"
    
    def __str__(self):
        return '%s @%s' % (self.event.event_id, self.log.computer.nickname)

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)