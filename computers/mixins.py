from django.contrib.auth.mixins import AccessMixin
from django.urls import reverse_lazy


class LoginRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        else:
            if not request.user.is_staff:
                return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def get_login_url(self):
        return reverse_lazy('com:login')
