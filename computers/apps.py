from django.apps import AppConfig


class ComputersConfig(AppConfig):
    name = 'computers'
    verbose_name = "Resources"