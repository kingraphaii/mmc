from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from .models import (
    Branch, Computer, Log, LogEntry, Event
)
from .tasks import process_log_file


admin.site.site_header = _('MMC Administration Panel')
admin.site.site_title = _('MMC Administration')
admin.site.index_title = _('MMC System Administration')


@admin.register(Branch)
class BrAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "created", "modified",)
    list_filter = ("created",)
    search_fields = ("name", "id")
    list_per_page  = 10


@admin.register(Computer)
class CoAdmin(admin.ModelAdmin):
    list_display = ("id", "serial", "computer_type", "branch", "created", "modified")
    list_filter = ("computer_type", "branch", "created")
    search_fields = ("serial", "nickname")
    list_per_page = 10


@admin.register(Log)
class LoAdmin(admin.ModelAdmin):
    list_display = ("id", 'computer', "start", "end", "file", "processed", "created", "modified")
    list_filter = ("computer__computer_type", "computer__branch")
    search_fields = ("id", "computer__nickname")
    list_per_page = 10

    actions = ['process_file',]

    def process_file(self, request, qs):
        for l in qs:
            process_log_file.delay(l.pk)

    process_file.short_description = "Process file"


@admin.register(LogEntry)
class LoeAdmin(admin.ModelAdmin):
    list_display = ("id", "log", "event", "timestamp", "created", "modified")
    list_filter = ("event", "log__computer__branch",)
    search_fields = ("event__event_id", "log__computer__nickname")
    list_per_page = 10


@admin.register(Event)
class EvAdmin(admin.ModelAdmin):
    list_display = ("event_id", "title", "color", "description", "created", "modified",)
    